# HandyPI

Raspberry PI network utilities
This script install & prepare Raspberry PI for network utilities

### Software list
- tshark
- iperf
- rsyslog
- cacti
- arpwatch
- snmpwalk
- phonehome (VPN)
- wifi (TBD)
